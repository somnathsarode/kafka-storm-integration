package com.kafkastorm;

/**
 * @author Somnath Sarode
 */
import backtype.storm.Config;
        import backtype.storm.LocalCluster;
        import backtype.storm.StormSubmitter;
        import backtype.storm.generated.AlreadyAliveException;
        import backtype.storm.generated.InvalidTopologyException;
        import backtype.storm.spout.SchemeAsMultiScheme;
        import backtype.storm.topology.TopologyBuilder;
        import storm.kafka.KafkaSpout;
        import storm.kafka.SpoutConfig;
        import storm.kafka.StringScheme;
        import storm.kafka.ZkHosts;

/**
 * This class connects to kafka machine in this case local machine then reads the messages from topic
 *
 */
public class Main
{
    public static void main( String[] args ) throws AlreadyAliveException, InvalidTopologyException
    {

        ZkHosts zkHosts=new ZkHosts("localhost:2181");

        String topic_name="vehicle";
        String consumer_group_id="id7";
        String zookeeper_root="";
        SpoutConfig kafkaConfig=new SpoutConfig(zkHosts, topic_name, zookeeper_root, consumer_group_id);

        kafkaConfig.scheme=new SchemeAsMultiScheme(new StringScheme());
        kafkaConfig.forceFromStart=true;

        TopologyBuilder builder=new TopologyBuilder();
        builder.setSpout("KafkaSpout", new KafkaSpout(kafkaConfig), 1);
        builder.setBolt("PrinterBolt", new PrinterBolt()).globalGrouping("KafkaSpout");

        Config config=new Config();
        config.setDebug(true);
        if (args != null && args.length > 0) {
            config.setNumWorkers(3);

            StormSubmitter.submitTopology(args[0], config, builder.createTopology());
        }
        else {



            LocalCluster cluster=new LocalCluster();

            cluster.submitTopology("KafkaConsumerTopology", config, builder.createTopology());
            try{
                Thread.sleep(60000);
            }catch(InterruptedException ex)
            {
                ex.printStackTrace();
            }

            cluster.killTopology("KafkaConsumerTopology");
            cluster.shutdown();
        }

    }
}

