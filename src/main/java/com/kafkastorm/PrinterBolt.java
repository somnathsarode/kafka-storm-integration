package com.kafkastorm;

/**
 * @author Somnath Sarode
 */
import org.apache.commons.lang.StringUtils;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/***
 *  Printer bolt writes to a file which further input for log stash
 */
public class PrinterBolt extends BaseBasicBolt {


    private static final Logger logger = LoggerFactory.getLogger(PrinterBolt.class);
    public void execute(Tuple input, BasicOutputCollector collector) {
        // TODO Auto-generated method stub
        String word=input.getString(0);
        if(StringUtils.isBlank(word))
        {
            return;
        }
        /**
         * Simply trying to file vehicles form Bellevue city
         */
        if(word.contains("\"city\":\"Bellevue\""))
        {

            //	System.out.println(word);
            try {
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("/tmp/klogs", true)));
                out.println(word);
                out.close();
            } catch (IOException e) {

            }

        }
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        // TODO Auto-generated method stub

    }

}

